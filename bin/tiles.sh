#!/bin/bash

HEIGHT=1080
WIDTH=2560
GAP=32

COUNTX=3
COUNTY=2

MARGINX=32
MARGINY=96
MARGINY_BOTTOM=32
GAP=32

ACTIVE=$(xdotool getactivewindow)

if [[ "$1" = "full" ]]; then
	FULL_WIDTH=$(($WIDTH - $GAP * 2 - $MARGINX))
	FULL_HEIGHT=$(($HEIGHT - 2 * $MARGINY_BOTTOM - 2 * $GAP))

	if [[ "$2" = "h" ]]; then
		xdotool windowmove $ACTIVE x $MARGINY
		xdotool windowsize $ACTIVE width $FULL_HEIGHT
		exit 0
	elif [[ "$2" = "w" ]]; then
		echo a
		xdotool windowmove $ACTIVE $MARGINX y
		xdotool windowsize $ACTIVE $FULL_WIDTH height
		exit 0
	fi

	xdotool windowmove $ACTIVE $MARGINX $MARGINY
	xdotool windowsize $ACTIVE $FULL_WIDTH $FULL_HEIGHT
	exit 0
fi

TILE_WIDTH=$((($WIDTH - 2 * $MARGINX) / $COUNTX - 2 * $GAP))
TILE_HEIGHT=$((($HEIGHT - 2 * $MARGINY_BOTTOM) / $COUNTY - 2 * $GAP))

POS_X=$(($1 * $TILE_WIDTH + $MARGINX + $1 * 2 * $GAP))
POS_Y=$(($2 * $TILE_HEIGHT + $MARGINY + $2 * 2 * $GAP))

xdotool windowmove --sync $ACTIVE $POS_X $POS_Y
xdotool windowsize $ACTIVE $TILE_WIDTH $TILE_HEIGHT
