#!/usr/bin/fish
function fish_prompt
	printf '%s λ ' (prompt_pwd)
end

function fish_right_prompt 
	printf '%s ' (disorg.sh COUNT)
	set_color -o magenta;
	printf '%s' (weather)
	set_color normal;
end

# cache weather for 5 mins to avoid murdering the guy's server
set WEATHER_NEXT_MIN "0"
set WEATHER "UNCACHED"
set CACHE_REFRESH_INTERVAL 5
function weather
	set TIME (date '+%M')
	if test "$TIME" -gt "$WEATHER_NEXT_MIN"
		set WEATHER (curl 'https://wttr.in/Denver?m&format=3' -s)
		set WEATHER_NEXT_MIN (math $TIME+$CACHE_REFRESH_INTERVAL)
	end
	printf "$WEATHER"
end
