set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'davidhalter/jedi-vim'
Plugin 'neoclide/coc.nvim'
call vundle#end()
filetype plugin indent on

" highlight lines of 80 chars
match ErrorMsg '\%>80v.\+'

" enable line numbers if more than 150 lines
autocmd BufReadPost * let &l:number = line('$') > 150

autocmd BufRead,BufNewFile *.htm,*.html,*.xhtml setlocal tabstop=2 shiftwidth=2 softtabstop=2

" spellcheck and text wrapping on docs (not source)
au BufReadPost,BufNewFile *.md,*.txt,*.tex set spell
au BufReadPost,BufNewFile *.md,*.txt,*.tex,*.rs set textwidth=80

" word wrapping
:set linebreak

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
