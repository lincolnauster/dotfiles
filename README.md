# openbox dotfiles
Minimal configuration of openbox with polybar

![screenshot](SCREENSHOT.png)

FILES:
* `themerc`: the openbox custom theme. Currently symlinked to
  `/usr/share/themes/Simple/openbox-3/themerc`
* `.xinitirc`: self-explanatory
* `.config/*`: All relavant subdirectories of ~/.config
